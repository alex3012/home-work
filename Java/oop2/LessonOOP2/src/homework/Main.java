package homework;

public class Main {

	public static void main(String[] args) { 
		Cat cat = new Cat("Fish", "Black", 5, "Tom");
		Dog dog = new Dog("Meat", "Brown", 30, "Duce");
		
		dog.eat();
		cat.eat();
		
		dog.sleep();
		cat.sleep();
		
		Veterinarian vet = new Veterinarian("Dr. Dave");
		
		vet.treatment(dog);
		vet.treatment(cat);

	}
}
