package homework2;

public class InverseTransformer extends TextTransformer{

	public InverseTransformer() {
		super();
	}

	public InverseTransformer(String text) {
		super(text);
	}
	
	public String transform(String text) {
		String reversedText = new StringBuffer(text).reverse().toString();
		return reversedText;
	}

	@Override
	public String toString() {
		return "InverseTransformer []";
	}
}
