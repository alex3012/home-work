package homework2;

public class TextTransformer {
	private String text;
	
	public TextTransformer(String text) {
		super();
		this.text = text;
	}

	public TextTransformer() {
		super();
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String transform(String text) {
		String textUpper = text.toUpperCase();
		return textUpper;
	}

	@Override
	public String toString() {
		return "TextTransformer [text=" + text + "]";
	}

}
