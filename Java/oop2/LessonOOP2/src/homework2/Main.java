package homework2;

import java.io.File;

public class Main {

	public static void main(String[] args) {
		TextSaver textSaver = new TextSaver(new UpDownTransformer(), new File("fileText.txt"));
		TextSaver textSaver1 = new TextSaver(new InverseTransformer(), new File("fileText1.txt"));
		TextSaver textSaver2 = new TextSaver(new TextTransformer(), new File("fileText2.txt"));
		
		UpDownTransformer text = new UpDownTransformer();
		InverseTransformer text1 = new InverseTransformer();
		TextTransformer text2 = new TextTransformer();
		
        textSaver.saveTextToFile("Good afternoon");
        textSaver1.saveTextToFile("Good afternoon");
        textSaver2.saveTextToFile("Good afternoon");
        
        System.out.println(text.transform("Good afternoon"));
        System.out.println(text1.transform("Good afternoon"));
        System.out.println(text2.transform("Good afternoon"));
	}

}
