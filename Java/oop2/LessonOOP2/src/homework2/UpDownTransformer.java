package homework2;

public class UpDownTransformer extends TextTransformer{

	public UpDownTransformer() {
		super();
	}

	public UpDownTransformer(String text) {
		super(text);
	}
	
	public String transform(String text) {
		StringBuilder sb = new StringBuilder(text);
	       for(int i = 0; i < text.length(); i++) {
	    	   if(i % 2 == 0) { 
	    		   sb.setCharAt(i, Character.toUpperCase(text.charAt(i)));
	    	   }else {
	    		   sb.setCharAt(i, Character.toLowerCase(text.charAt(i)));
	    	   }
	       }
		return sb.toString();
	}

	@Override
	public String toString() {
		return "UpDownTransformer []";
	}

}
