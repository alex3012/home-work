package cw; 

public class Main {

  public static void main(String[] args) {
    // TODO Auto-generated method stub
    Employee employee1 = new Employee("Alex", "Ts", 0, "Sci dev");

    System.out.println(employee1.getName());

    System.out.println(employee1.hashCode());

    System.out.println(employee1);

    Person person1 = employee1;

    System.out.println(person1);

    System.out.println(person1.getClass());

    if (person1.getClass().equals(Employee.class)) {

      Employee employee2 = (Employee) person1;
    }

  }

}
