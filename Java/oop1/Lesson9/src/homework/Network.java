package homework;

import java.util.*;

class Network {
	private ArrayList<Phone> registeredPhones = new ArrayList<>();
	
	public Network(ArrayList<Phone> numbers) {
		super();
		this.registeredPhones = registeredPhones;
	}

	public Network() {
		super();
	}

	public ArrayList<Phone> getRegisteredPhones() {
		return registeredPhones;
	}

	public void setRegisteredPhones(ArrayList<Phone> registeredPhones) {
		this.registeredPhones = registeredPhones;
	}

	public boolean registerPhone(Phone phone) {
		if (!registeredPhones.contains(phone)) {
			registeredPhones.add(phone);
			return true;
		}
		return false;
	}

	public boolean isNumberRegistered(String number) {
		for (Phone phone : registeredPhones) {
			if (phone.getNumber().equals(number)) {
				return true;
			}
		}
		return false;
	}

	public void receiveCall(String callingNumber, String targetNumber) {
		for (Phone phone : registeredPhones) {
			if (phone.getNumber().equals(targetNumber)) {
				phone.receiveCall(callingNumber);
				break;
			}
		}
	}

	@Override
	public String toString() {
		return "Network [registeredPhones=" + registeredPhones + "]";
	}
}
