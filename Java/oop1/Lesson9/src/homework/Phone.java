package homework;

public class Phone {
    private String number;
    private boolean isRegistered;
    private Network network;
    
    public Phone(String number, Network network) {
		super();
		this.number = number;
		this.network = network;
		this.isRegistered = false;
	}

	public Phone() {
		super();
	}

	public boolean isRegistered() {
		return isRegistered;
	}

	public void setRegistered(boolean isRegistered) {
		this.isRegistered = isRegistered;
	}

	public void registerToNetwork(Network network) {
        if (network.registerPhone(this)) {
            isRegistered = true;
            System.out.println("Phone " + getNumber() + " successfully registered on the network.");
        } else {
            System.out.println("Registration error. Phone " + getNumber() + " cannot be registered on the network.");
        }
    }

    public void makeCall(String targetNumber) {
        if (!isRegistered) {
            System.out.println("Registration error. Phone " + getNumber() + " cannot be registered on the network.");
            return;
        }

        if (network.isNumberRegistered(targetNumber)) {
            System.out.println("Outgoing call from number " + getNumber() + " to number " + targetNumber);
            network.receiveCall(targetNumber, this.getNumber());
        } else {
            System.out.println("Call error. Number " + targetNumber + " is not registered on the network.");
        }
    }

    public void receiveCall(String callingNumber) {
        System.out.println("A number is calling you " + callingNumber);
    }

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return "Phone [number=" + number + ", isRegistered=" + isRegistered + ", network=" + network + "]";
	}
	
}

		



