package homework;

public class Main3 {
    public static void main(String[] args) {
        Network network = new Network();

        Phone phone1 = new Phone("123456789", network);
        Phone phone2 = new Phone("987654321", network);
        Phone phone3 = new Phone("111111111", network);

        phone1.registerToNetwork(network);
        phone2.registerToNetwork(network);

        phone1.makeCall("987654321"); 
        phone2.makeCall("111111111"); 

        
        phone1.receiveCall("888888888");
        phone1.receiveCall("111111111");
    }
}
