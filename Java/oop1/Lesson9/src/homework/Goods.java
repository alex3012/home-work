package homework;

public class Goods {
	private int price;
	private int weight;
	private String description;
	private String name;
	
	
	
	public Goods(int price, int weight, String description, String name) {
		super();
		this.price = price;
		this.weight = weight;
		this.description = description;
		this.name = name;
	}

	public Goods() {
		super();
	}

	

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String discount() {
		return description + " discount 20 %";
	}


	@Override
	public String toString() {
		return "Goods [price=" + price + ", weight=" + weight + ", description=" + description + ", name=" + name + "]";
	}
}