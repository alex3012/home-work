package homework;

public class Main2 {
	public static void main(String[] args) {
		Triangle triangle1 = new Triangle(4.4, 5.2, 3.7);
		Triangle triangle2 = new Triangle(7, 6.9, 10.5);
		
		System.out.println(triangle1.square());
		System.out.println(triangle2.square());
		
		System.out.println(triangle1);
		System.out.println(triangle2);
	}

}
