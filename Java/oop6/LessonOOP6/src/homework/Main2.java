package homework;

import java.util.Random;

public class Main2 {
	public static void main(String[] args) {
		int[] arr = new int[100000000];
		Random random = new Random();
		for (int i = 0; i < arr.length; i++) {
			arr[i] = random.nextInt(1001);
		}
		
		

	    long t1 = System.nanoTime();
	    System.out.println("Sum 1 stream : " + sumArray(arr));
	    long t2 = System.nanoTime();
	    System.out.println((t2 - t1) + " ns");
	    
	    System.out.println();
	    System.out.println("Sum some streams");
	    
	    long t3 = System.nanoTime();
	    SumMultiThread smt = new SumMultiThread(arr, 4);
	    smt.creatArrays();
	    long t4 = System.nanoTime();
	    System.out.println((t4 - t3) + " ns");
	    
	    
	}
	
	

	public static long sumArray(int[] arr) {
		long sum = 0;
		for (int i = 0; i < arr.length; i++) {
			sum += arr[i];
		}
		return sum;
	}

}
