package homework;

public class SumMultiThread {
	private int[] array;
    private int numThread;
    
	public SumMultiThread(int[] array, int numThread) {
		super();
		this.array = array;
		this.numThread = numThread;
	}
	
	public SumThread[] creatArrays(){
		int part = array.length/numThread;
		SumThread[] st = new SumThread[numThread];
		for(int i = 0; i< numThread; i++) {
			int startIndex = i * part;
            int endIndex = (i == numThread - 1) ? array.length : (i + 1) * part;
            st[i] = new SumThread(array, startIndex, endIndex);
            Thread sti = new Thread(st[i]);
            sti.start();
		}
		return st;
		
	}
    
    

}
