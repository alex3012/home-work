package homework;

import cw.FactorialTask;

public class Main {

	public static void main(String[] args) {

		FactorialTask[] task = new FactorialTask[100];
		Thread[] thread = new Thread[100];

		for (int i = 0; i < task.length; i++) {
			task[i] = new FactorialTask(i);
			thread[i] = new Thread(task[i]);
			thread[i].start();
		}
	}
}
