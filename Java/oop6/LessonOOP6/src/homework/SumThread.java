package homework;

public class SumThread implements Runnable{
	private int[] array;
    private int startIndex;
    private int endIndex;
    private long partialSum = 0;
	
	public SumThread(int[] array, int startIndex, int endIndex) {
		super();
		this.array = array;
		this.startIndex = startIndex;
		this.endIndex = endIndex;
	}

	public long getPartialSum() {
		return partialSum;
	}

	@Override
	  public void run() {
        for (int i = startIndex; i < endIndex; i++) {
        	partialSum += array[i];
        }
        System.out.println(partialSum);
	}
	
}
