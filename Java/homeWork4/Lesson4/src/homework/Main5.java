package homework; 
 
import java.util.Scanner; 
 
public class Main5 { 
 
  public static void main(String[] args) { 
    Scanner sc = new Scanner(System.in); 
    int width; 
    
    System.out.println("Enter an odd number "); 
    width = sc.nextInt();
    
    if(width%2 != 0) {
    	for (int i = 0; i <= width/2; i++) {
    	    for (int j = 0; j < i; j++) {
    		    System.out.print(" ");	
    	    }
    	    for (int k = 0; k < width-2*i; k++) {
    			System.out.print("*");
    	    }
    	    System.out.println();
        } 
        for (int i = width/2-1; i >= 0; i--) {
    	    for (int j = 0; j < i; j++) {
    		    System.out.print(" ");
    	    }
    	    for (int j = 0; j < width-2*i; j++) {
    		    System.out.print("*");
    	    }
    	    System.out.println();
        }
    }else {
    	System.out.println("Number is wrong");
    } 
  } 
}