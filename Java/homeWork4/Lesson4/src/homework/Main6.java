package homework; 
 
import java.util.Scanner; 
 
public class Main6 { 
 
  public static void main(String[] args) { 
    Scanner sc = new Scanner(System.in); 
    double number;
    double err = 0.01;
    
    System.out.println("Enter number "); 
    number = sc.nextDouble();
    
    double x0 = number;
    double x1 = 1 / 2.0 * (x0 + number / x0);
    double d = Math.abs(x1 - x0);
    
    for(; d >= 2 * err && d * d >= 2 * err;) {
    	x0 = x1;
    	x1 = 1 / 2.0 * (x0 + number / x0);
    	d = Math.abs(x1 - x0);
    }
    System.out.println("The root of the number " + number + " is : " + x1 );
  }
}  
    
   