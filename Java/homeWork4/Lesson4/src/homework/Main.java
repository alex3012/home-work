//Факториал числа

package homework;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
	    int n;
	    int res;

	    System.out.println("Enter a number from 4 to 16.");
	    n = sc.nextInt();
	    res = 1;
	    if(n<=4 || n>=16) {
	    	System.out.println("You entered an invalid number.");
	    }else {
	    	for(int i = n; i >= 1; i--) {
	    		res = res*i;	
	    	}
	    	System.out.println(n + "! = " + res + ".");
	    }

	}

}
