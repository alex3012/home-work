package homework;

import java.util.Scanner;

public class Main4 {
	 public static void main(String[] args) {
	        Scanner sc = new Scanner(System.in);

	        System.out.println("Enter height: ");
	        int height = sc.nextInt();

	        int count = 1;

	        for (int i = 1; i <= height; i++) {
	            System.out.print("*");
	            if (count <= i && count < height) {
	                System.out.println();
	                count++;
	                i = 0;
	            }
	            if (height == i) {
	                System.out.println();
	                height--;
	                i = 0;
	            }

	        }
	    }


}
