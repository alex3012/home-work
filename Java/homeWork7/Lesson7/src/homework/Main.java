package homework;

public class Main {

	public static void main(String[] args) {
		
		int[] arr = new int[] {1, 6, 0, -5, 4};
		
		int max = findMax(arr);
		
		System.out.println(max);
	}

	private static int findMax(int[] arr) {
		int currentMax = arr[0];
		for(int i = 0; i < arr.length; i++) {
			if(arr[i] > currentMax) {
				currentMax = arr[i];
			}
		}
		return currentMax;
	}

}
