package homework;

import java.util.Scanner;

public class Main4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a sequence of numbers separated by commas:");
        String input = scanner.nextLine();
        String[] numbersStr = input.split(",");
        
        double[] numbers = new double[numbersStr.length];
        for (int i = 0; i < numbersStr.length; i++) {
            numbers[i] = Double.parseDouble(numbersStr[i]);
        }
        
        if (isArithmeticProgression(numbers)) {
            double next = numbers[numbers.length - 1] + (numbers[1] - numbers[0]);
            System.out.println("Next member of the sequence: " + next);
        } else if (isGeometricProgression(numbers)) {
            double next = numbers[numbers.length - 1] * (numbers[1] / numbers[0]);
            System.out.println("Next member of the sequence: " + next);
        } else if (isPowerSeries(numbers)) {
            double[] logarithms = new double[numbers.length];
            for (int i = 0; i < numbers.length; i++) {
                logarithms[i] = Math.log(numbers[i]);
            }
            
            if (isArithmeticProgression(logarithms)) {
                double nextLog = logarithms[logarithms.length - 1] + (logarithms[1] - logarithms[0]);
                double next = Math.exp(nextLog);
                System.out.println("Next member of the sequence: " + next);
            } else {
                System.out.println("The sequence is not a power series.");
            }
        } else {
            System.out.println("The sequence is neither an arithmetic nor a geometric progression.");
        }
    }
    
    public static boolean isArithmeticProgression(double[] sequence) {
        double num = sequence[1] - sequence[0];
        for (int i = 2; i < sequence.length; i++) {
            if (sequence[i] - sequence[i - 1] != num) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean isGeometricProgression(double[] sequence) {
        double num = sequence[1] / sequence[0];
        for (int i = 2; i < sequence.length; i++) {
            if (sequence[i] / sequence[i - 1] != num) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean isPowerSeries(double[] sequence) {
        for (double num : sequence) {
            if (num <= 0) {
                return false;
            }
        }
        return true;
    }
}
