package homework;

import java.util.Arrays;

public class Main6 {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5};
        generateCombinations(array, new int[array.length], 0, new boolean[array.length]);
    }

    public static void generateCombinations(int[] array, int[] currentCombination, int currentIndex, boolean[] used) {
        if (currentIndex == array.length) {
            System.out.println(Arrays.toString(currentCombination));
            return;
        }

        for (int i = 0; i < array.length; i++) {
            if (!used[i]) {
                currentCombination[currentIndex] = array[i];
                used[i] = true;
                generateCombinations(array, currentCombination, currentIndex + 1, used);
                used[i] = false;
            }
        }
    }
}
