package homework;

public class Main5 {
	public static void main(String[] args) {
        int result = findLargestPalindrome();
        System.out.println(""
        		+ "Largest palindrome: " + result);
    }
    
    public static int findLargestPalindrome() {
        int largestPalindrome = 0;
        
        for (int i = 999; i >= 100; i--) {
            for (int j = 999; j >= 100; j--) {
                int number = i * j;
                if (number < largestPalindrome) {
                    break;
                }
                
                if (isPalindrome(number) && number > largestPalindrome) {
                    largestPalindrome = number;
                }
            }
        }
        
        return largestPalindrome;
    }
    
    public static boolean isPalindrome(int number) {
        String numStr = String.valueOf(number);
        int left = 0;
        int right = numStr.length() - 1;
        
        while (left < right) {
            if (numStr.charAt(left) != numStr.charAt(right)) {
                return false;
            }
            left++;
            right--;
        }
        
        return true;
    }
}
