package homework;

public class Main3 {
	public static void main(String[] args) {
		int[] sequence = new int[] {-3, 0, 3, 6, 8, 12, 13, 16, 29};
		int element = 6;
		
		System.out.println(linearSearch(sequence, element));
	}

	private static int linearSearch(int[] sequence, int element) {
		for(int i = 0; 1 < sequence.length; i++) {
			if(sequence[i] == element) {
				return i;
			}
		}
		return -1;
	}

}
