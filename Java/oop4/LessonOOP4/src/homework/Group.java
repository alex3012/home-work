package homework;

import java.util.Arrays;

import java.util.Comparator;

import homework.exception.GroupOverflowException;
import homework.exception.StudentNotFoundException;

public class Group {
	private String groupName;
	private final Student[] students;

	public Group(String groupName) {
		super();
		this.groupName = groupName;
		students = new Student[10];
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Student[] getStudents() {
		return students;
	}

	public void addStudent(Student student) throws GroupOverflowException {
		for (int i = 0; i < students.length; i++) {
			if (students[i] == null) {
				students[i] = student;
				return;
			}
		}
		throw new GroupOverflowException("Group is full. Cannot add more students.");
	}

	public Student searchStudentByLastName(String lastName) throws StudentNotFoundException {
		for (int i = 0; i < students.length; i++) {
			if (students[i] != null) {
				if (students[i].getLastName().equalsIgnoreCase(lastName)) {
					return students[i];
				}
			}
		}
		throw new StudentNotFoundException("Student with last name " + lastName + " not found in the group.");
	}

	public boolean removeStudentByID(int id) {
		for (int i = 0; i < students.length; i++) {
			if (students[i] != null) {
				if (students[i].getId() == id) {
					students[i] = null;
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public String toString() {
		Arrays.sort(students, Comparator.nullsLast(new StudentLastNameComparator()));
		String result = "Group" + System.lineSeparator();
		for (int i = 0; i < students.length; i++) {
			if (students[i] != null) {
				result += students[i] + System.lineSeparator();
			}
		}

		return result;
		// return "Group [groupName=" + groupName + ", students=" +
		// Arrays.toString(students) + "]";
	}

}

