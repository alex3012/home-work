package homework;

import homework.exception.GroupOverflowException;
import homework.exception.InvalidGenderException;
import homework.exception.InvalidIdException;
import homework.exception.StudentNotFoundException;

public class Main {

	public static void main(String[] args) {
		Student student1 = new Student("Djon", "BOW", Gender.MALE, 1, "BACK-END");
		Student student2 = new Student("Dik", "Svenson", Gender.MALE, 3, "BACK-END");
		Student student3 = new Student("Anna", "Smitt", Gender.FEMALE, 9, "BACK-END");
		Student student4 = new Student("Alan", "Po", Gender.MALE, 6, "BACK-END");
		Student student5 = new Student("Ray", "Bredbery", Gender.MALE, 8, "BACK-END");
		Student student6 = new Student("Don", "Nilson", Gender.OTHER, 7, "BACK-END");
		Student student7 = new Student("Janna", "Vo", Gender.FEMALE, 5, "BACK-END");
		Student student8 = new Student("Patrik", "Zimmermann", Gender.MALE, 4, "BACK-END");
		Student student9 = new Student("Alex", "Niman", Gender.MALE, 10, "BACK-END");
		Student student10 = new Student("Mik", "Tarkov", Gender.MALE, 2, "BACK-END");
		
		Group group = new Group("BACK-END");
		
		try {
			group.addStudent(student1);
			group.addStudent(student2);
			group.addStudent(student3);
			group.addStudent(student4);
			group.addStudent(student5);
			group.addStudent(student6);
			group.addStudent(student7);
			group.addStudent(student8);
			group.addStudent(student9);
			group.addStudent(student10);
			
		} catch (GroupOverflowException e) {
			e.printStackTrace();
		}
		
		
		// CSV конвертер 
		CSVStringConverter csvStrC = new CSVStringConverter();
		String csvR = csvStrC.toStringRepresentation(student4);
		System.out.println("CSV Representation: " + csvR);
		
		System.out.println(group.removeStudentByID(6));
		System.out.println(group.removeStudentByID(7));
		
		Student newStudent = csvStrC.fromStringRepresentation(csvR);
		try {
			group.addStudent(newStudent);
		} catch (GroupOverflowException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Студент из консоли
		try {
			group.addStudent(StudentInputReader.readStudentInput());
		} catch (GroupOverflowException e) {
			e.printStackTrace();
		} catch (InvalidGenderException e) {
			e.printStackTrace();
		} catch (InvalidIdException e) {
			e.printStackTrace();
		}
		
		System.out.println();
		System.out.println(group);
	}
}

