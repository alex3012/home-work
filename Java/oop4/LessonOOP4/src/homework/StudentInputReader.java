package homework;

import java.util.InputMismatchException;
import java.util.Scanner;

import homework.exception.InvalidGenderException;
import homework.exception.InvalidIdException;

public class StudentInputReader {

	public static Student readStudentInput() throws InvalidGenderException, InvalidIdException {
		Scanner sc = new Scanner(System.in);
		Student student = new Student();
		
		System.out.print("Enter student's name: ");
        student.setName(sc.nextLine());

        System.out.print("Enter student's last name: ");
        student.setLastName(sc.nextLine());

        System.out.print("Enter student's gender (MALE, FEMALE or OTHER): ");
        try {
        	student.setGender(Gender.valueOf(sc.nextLine().toUpperCase()));
        } catch (IllegalArgumentException e) {
            throw new InvalidGenderException("Invalid gender.");
        }
        
        System.out.print("Enter student's ID: ");
        try {
            student.setId(sc.nextInt());
        } catch (InputMismatchException e) {
            throw new InvalidIdException("Invalid ID.");
        }

        System.out.println("Enter student's group name: ");
        String groupName = sc.nextLine();
        student.setGroupName(sc.nextLine());

        return student;
	}
}
