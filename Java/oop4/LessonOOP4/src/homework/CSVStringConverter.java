package homework;

public class CSVStringConverter implements StringConverter {

	@Override
	public String toStringRepresentation(Student student) {
		String del = ";";
		String str = student.getName() + del + student.getLastName() + del + student.getGender() + del + student.getId()
				+ del + student.getGroupName() + del;
		return str;
	}

	@Override
	public Student fromStringRepresentation(String str) {
		String[] parts = str.split(";");
		if (parts.length != 5) {
			throw new IllegalArgumentException("Invalid CSV string format.");
		}

		String name = parts[0];
		String lastName = parts[1];
		Gender gender = Gender.valueOf(parts[2]);
		int id = Integer.parseInt(parts[3]);
		String groupName = parts[4];

		return new Student(name, lastName, gender, id, groupName);
	}

}
