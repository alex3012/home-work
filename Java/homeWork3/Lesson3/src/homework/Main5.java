package homework;

import java.util.Scanner;

public class Main5 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int number;
		int sign1;
		int sign2;
		int sign3;
		int sign4;
		int sign5;
		int sign6;
		
		System.out.println("Input any 6 digit number.");
	    number = sc.nextInt();
	    
	    sign1 = number/100000;
	    sign2 = (number%100000)/10000;
	    sign3 = (number%10000)/1000;
	    sign4 = (number%1000)/100;
	    sign5 = (number%100)/10;
	    sign6 = number%10;
	    
	    if(number<100000 || number>999999) {
	    	System.out.println("You entered an invalid number.");
	    }else if(sign1==sign6 && sign2==sign5 && sign3==sign4) {
	    	System.out.println("Number " + number + " is palindrome.");
	    }else {
	    	System.out.println("Number " + number + " is not palindrome.");
	    }

	}

}
