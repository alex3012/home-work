package homework;

import java.util.Scanner;

public class Main7 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double xO;
		double yO;
		double xA = 0;
		double yA = 0;
		double xB = 4;
		double yB = 4;
		double xC = 6;
		double yC = 1;
		
		System.out.println("Enter x coordinate.");
	    xO = sc.nextDouble();
	    System.out.println("Enter y coordinate.");
	    yO = sc.nextDouble();
	    
	    double vector_xAB = xB - xA;
	    double vector_yAB = yB - yA;
	    double vector_xAO = xO - xA;
	    double vector_yAO = yO - yA;
	    double resA = vector_xAB*vector_yAO - vector_yAB*vector_xAO;
	    
	    double vector_xBC = xC - xB;
	    double vector_yBC = yC - yB;
	    double vector_xBO = xO - xB;
	    double vector_yBO = yO - yB;
	    double resB = vector_xBC*vector_yBO - vector_yBC*vector_xBO;
	    
	    double vector_xCA = xA - xC;
	    double vector_yCA = yA - yC;
	    double vector_xCO = xO - xC;
	    double vector_yCO = yO - yC;
	    double resC = vector_xCA*vector_yCO - vector_yCA*vector_xCO;
	    
	    if((resA >= 0 && resB >= 0 && resC >= 0) || (resA <= 0 && resB <= 0 && resC <= 0)) {
	    	System.out.println("The point with coordinates (" + xO + "," + yO + ") is inside the triangle.");
	    }else {
	    	System.out.println("The point with coordinates (" + xO + "," + yO + ") is not inside the triangle.");
	    }

	}

}
