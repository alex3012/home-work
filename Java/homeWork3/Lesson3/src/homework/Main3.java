package homework;

import java.util.Scanner;

public class Main3 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int a;
	    int b;
	    int c;

	    System.out.println("Input side a");
	    a = sc.nextInt();
	    System.out.println("Input side b");
	    b = sc.nextInt();
	    System.out.println("Input side c");
	    c = sc.nextInt();
	    
	    if(a+b>c && b+c>a && c+a>b) {
	    	System.out.println("This triangle exists");
	    }else {
	    	System.out.println("No such triangle exists");
	    }

	}

}
