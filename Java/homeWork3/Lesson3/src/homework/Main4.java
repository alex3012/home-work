package homework;

import java.util.Scanner;

public class Main4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int number;
		int sign1;
		int sign2;
		int sign3;
		int sign4;
		
		System.out.println("Input any 4 digit number.");
	    number = sc.nextInt();
	    
	    sign1 = number/1000;
	    sign2 = (number%1000)/100;
	    sign3 = (number%100)/10;
	    sign4 = number%10;
	    
	    if(number<1000 || number>9999) {//Не могу проверить количество знаков. Цифры например 0101 не входят в диапазон.
	    	System.out.println("You entered an invalid number.");
	    }else if((sign1 + sign2) == (sign3 + sign4)) {
	    	System.out.println("Luck! Number " + number + " is lucky.!");
	    }else {
	    	System.out.println("Unfortunately, number " + number + " is not lucky.");
	    }

	}

}
