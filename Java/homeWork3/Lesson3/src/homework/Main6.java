package homework;

import java.util.Scanner;

public class Main6 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double x;
		double y;
		double R = 4;
		
		System.out.println("Enter x coordinate.");
	    x = sc.nextDouble();
	    System.out.println("Enter y coordinate.");
	    y = sc.nextDouble();
	    
	    if((Math.pow(x,2) + Math.pow(y,2)) <= Math.pow(R,2)) {
	    	System.out.println("The point with coordinates (\" + x + \",\" + y + \") is inside the circle.");
	    }else {
	    	System.out.println("The point with coordinates (" + x + "," + y + ") is not inside the circle.");
	    }
	}
}
