package homework;

import java.util.Scanner;

public class Main2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int number;
		int floor;
		int entrance;
		
		System.out.println("Input number");
	    number = sc.nextInt();
	    
	    entrance = ((number - 1)/36) + 1;
	    floor = ((number - 36*((number -1)/36) - 1)/4) + 1;
	    
	    if(entrance >5 || number == 0) {
	    	System.out.println("This apartment does not exist");
	    }else {
	    	System.out.println("Apartment - " + number + ", entrance - " + entrance + ", floor - " + floor);
	    }
	    
	    
	}

}
