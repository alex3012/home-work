package homework;

public class Main3 {
	public static void main(String[] args) {
		double num = Math.PI;
		
		numberSigns(num);
	}
	public static void numberSigns(double number) {
		for(int i = 2; i <= 11; i++) {
			String text = String.format("Pi number : %." + i + "f", number);
		      System.out.println(text);
		}
	}
}
