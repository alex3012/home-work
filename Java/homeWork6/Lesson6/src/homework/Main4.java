package homework;

import java.util.Scanner;

public class Main4 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
        System.out.println("How much money do you have? (in the format: dollars,cents)");
        
        String text = sc.nextLine();
        
        if (!text.matches("^[0-9]*[,]?[0-9]{0,2}$")) {
            System.out.println("Invalid input");
            return;
        }
        
        String[] result = text.split("[,]");

        long dollar = Long.parseLong(result[0]);
        if(dollar >= 1000000000) {
        	System.out.println("You are too rich.");
        	return;
        }
        
        long cent = 0;
        if(result.length != 1) {
        	cent = Long.parseLong(result[1]);
        	if(cent > 0 && cent <=9 && result[1].length() != 2) {
            	cent *= 10;
            }
        }
        
        
        String dollars = convertQuantity(dollar);
        String cents = convertQuantity(cent);

        System.out.println("You have: " + dollars + " dollars " + cents + " cents.");
    }
	
	public static String unit(long q) {
		String[] units = new String[]{"zero", "one", "two", "three", "four", "five", "six", "seven",
        		                      "eight", "nine", "ten", "eleven", "twelve", "thirteen",
        		                      "fourteen", "fifteen", "sixteen", "seventeen", "eighteen",
        		                      "nineteen"};
		return units[(int) q];
	}
	
	public static String ten(long q) {
		String[] tens = new String[]{"", "", "twenty", "thirty", "forty", "fifty",
        		                     "sixty", "seventy", "eighty", "ninety"};
		return tens[(int) q];
	}
	
	public static String convertQuantity(long q) {
		if (q < 20) {
            return unit(q);
        }

        if (q < 100) {
        	if(q % 10 == 0) {
        		return ten(q / 10);
        	}
            return ten(q / 10) + " " + unit(q % 10);
        }

        if (q < 1000) {
            return unit(q / 100) + " hundred " + convertQuantity(q % 100);
        }

        if (q < 1000000) {
            return convertQuantity(q / 1000) + " thousand " + convertQuantity(q % 1000);
        }

        if (q < 1000000000) {
            return convertQuantity(q / 1000000) + " million " + convertQuantity(q % 1000000);
        }
        return "";
	}

}
