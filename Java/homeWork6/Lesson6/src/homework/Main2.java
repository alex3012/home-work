package homework;

import java.util.Scanner;

public class Main2 {
	public static void main(String[] args) { 
		
	    Scanner sc = new Scanner(System.in);
	    String text;
	    System.out.println("Input text: ");
	    text = sc.nextLine();
	    String[] result = text.split(" ");
	    
	    int res1 = maxWord(result);
	    System.out.println(res1);
	}
	public static int maxWord(String[] result) {
		
		int num = result[0].length();
		for (int i = 1; i < result.length; i++ ) {
	         if(result[i].length() > num) {
	        	 num = result[i].length();
	         }
	         //System.out.println(result[i].length());
	         
	    }
		return num;
	}
}
