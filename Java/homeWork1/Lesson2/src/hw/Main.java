package hw;

public class Main {

	public static void main(String[] args) {
		
		/*  Написать программу которая вычислит и выведет на экран площадь треугольника если 
известны его стороны (sideA = 0.3, sideB = 0.4, sideC = 0.5). Для вычисления 
использовать формулу Герона.*/
		
        double saideA = 0.3;
        double saideB = 0.4;
        double saideC = 0.5;
        double p;
        double S;
        p = (saideA + saideB +saideC)/2;
        S = Math.sqrt(p*(p-saideA)*(p-saideB)*(p-saideC));
        System.out.println("area of a triangle: " + S);
        
        /*  Стоимость яблока составляет 2$. Покупатель приобретает 6 яблок. Напишите 
программу которая вычислит и выведет на экран сумму которую должен уплатить 
покупатель за покупку.  */
        
        double price = 2;
        int number = 6;
        System.out.println("sum: " + price*number + " $");
        
        /*   Один литр топлива стоит 1.2$. Ваш автомобиль тратит на 100 км пути 8 литров топлива. 
Вы собрались в поездку в соседний город. Расстояние до этого города составляет 120 
км. Вычислите и выведите на экран сколько вам нужно заплатить за топливо для 
поездки.   */
        
        double priceGas = 1.2;
        double costGas = 8;
        double distance = 120;
        double sumGas = priceGas*costGas*distance/100;
        System.out.println("A trip of " + distance + " km will cost you " + sumGas + " $");
	}

}
