package cw;



import java.io.File;
import java.io.IOException;

public class Main {

  public static void main(String[] args) {
    // TODO Auto-generated method stub
	  
	File file1 = new File("new_file.txt");
	
	try {
		file1.createNewFile();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 

    File workFolder = new File(".");
    File[] files = workFolder.listFiles();
    for (int i = 0; i < files.length; i++) {
      String type = "File";
      if (files[i].isDirectory()) {
        type = "Folder";
      }
      System.out.println(files[i] + "\t" + files[i].length() + "\t" + type);
    }

  }

}

