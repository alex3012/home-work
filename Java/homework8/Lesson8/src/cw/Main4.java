package cw;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

public class Main4 {
	public static void main(String[] args) {
		File file = new File("result.txt");
		
		int [] arr = new int[] {3,5,5,6,8};
		
		saveArray(file, arr);
		
		
		
	}
	
	public static void saveArray(File file, int[] array) {
		
		try (PrintWriter pw1 = new PrintWriter(file)) {
			pw1.println(Arrays.toString(array));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
