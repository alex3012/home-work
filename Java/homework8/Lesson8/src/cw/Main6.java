package cw;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class Main6 {

  public static void main(String[] args) {
    String[] goods = new String[] { "Snikers", "Mars", "Kit-Kat", "Kontik" };
    int[] prices = new int[] { 30, 30, 25, 15 };
    int[] numbers = new int[] { 120, 100, 110, 230 };

    File file = new File("report.csv");
    String del = ",";
    
    saveReport(file, del, goods, prices, numbers);
  }

  public static void saveReport(File file, String del, String[] goods, int[] prices, int[] numbers) {
    try (PrintWriter pw = new PrintWriter(file)) {

      for (int i = 0; i < numbers.length; i++) {
        pw.println(goods[i] + del + prices[i] + del + numbers[i]);
      }

    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
