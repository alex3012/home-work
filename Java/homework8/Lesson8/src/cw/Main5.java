package cw;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class Main5 {

  public static void main(String[] args) {

    File file = new File("result.txt");

    int[] arr = new int[] { 4, -2, 10, 0, 5, 7 };

    saveArray(file, arr);

  }

  public static void saveArray(File file, int[] array) {
    try (PrintWriter pw = new PrintWriter(file)) {
      for (int i = 0; i < array.length; i++) {
        pw.println(array[i]);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
