package homework;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

public class Main2 {
	public static void main(String[] args) {
		File file = new File("result.txt");
		
		int [][] arr = {{3, 4, 6}, {5, 3, 1}, {9, 6, 8}};
		
		saveArray(file, arr);	
	}
	
	public static void saveArray(File file, int[][] arr) {
		
		try (PrintWriter pw1 = new PrintWriter(file)) {
			pw1.println(Arrays.deepToString(arr));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
 
