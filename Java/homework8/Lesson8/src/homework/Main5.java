package homework;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.File;

public class Main5 {

	public static void main(String[] args) {
		File file = new File("art.txt");

		String line = generateArt();

		writeToFile(file, line);
	}

	public static String generateArt() {
		StringBuilder art = new StringBuilder();

		for (int i = 0; i < 40; i++) {
			for (int j = 0; j < 40; j++) {
				// char r = (char)Math.round(Math.random()*126);
				char symbol = (Math.random() < 0.6) ? '#' : ' ';
				art.append(symbol);
			}
			art.append('\n');
		}
		return art.toString();
	}

	public static void writeToFile(File file, String line) {
		try (PrintWriter pw = new PrintWriter(file)) {
			System.out.println("ASCII-art successfully created in file " + file);
			pw.println(line);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
