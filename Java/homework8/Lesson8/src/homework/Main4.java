package homework;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;

public class Main4 {

	public static void main(String[] args) {
		File file = new File("new_file.txt");

		String line = getStringFromFile(file);
		String line1 = line.toLowerCase().replaceAll("[^a-z]", "");

		String del = "-";

		String result = sortString(del, line1);

		System.out.println(line);
		System.out.println(result);
		
		String[] result1 = result.split("[:]");
		Arrays.sort(result1);
		reverse(result1);
		System.out.println(Arrays.toString(result1));
	}
	
	public static void reverse(String[] arr) {
        if (arr == null) {
            return;
        }
 
        for(int i = 0; i < arr.length / 2; i++) {
            String temp = arr[i];
            arr[i] = arr[arr.length - i - 1];
            arr[arr.length - i - 1] = temp;
        }
    }

	private static String sortString(String del, String line) {
		int num = 0;
		String r;
		String result = "";
		for (int i = 0; i < line.length(); i++) {
			r= line.substring(0,1);
			
			if(line.substring(i,i+1).equals(r)) {
				num += 1;
			}
			if(i == line.length() - 1) {
				result += num + del + r + ":";
				num = 0;
				line = line.replaceAll(r, "");
				i = -1;
			}	
		}
		return result.substring(0, result.length() - 1);
	}

	

	public static String getStringFromFile(File file) {
		String result = "";
		try (Scanner sc = new Scanner(file)) {
			for (; sc.hasNextLine();) {
				result += sc.nextLine() + System.lineSeparator();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result.substring(0, result.length() - 1);
	}

}
