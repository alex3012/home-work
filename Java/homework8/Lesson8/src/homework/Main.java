package homework;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		
        System.out.println("Simple text editor.");
        System.out.println("Enter text. To save, enter \":q\"");
        String text = "";
        
        inputText(text);
	}

	private static void inputText(String text) {
		while (true) {
			Scanner sc = new Scanner(System.in);
            String line = sc.nextLine();

            if (line.equals(":q")) {
            	System.out.println("Enter a filename to save: ");
            	
            	saveFile(text);
            	
                break;
            }
            text = text + line + "\n";  
        }	
	}

	private static void saveFile(String text) {
		Scanner sc = new Scanner(System.in);
        String fileName = sc.nextLine();
        
        File file = new File(fileName + ".txt");
        
        try (PrintWriter pw1 = new PrintWriter(file)) {
			pw1.println(text);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
