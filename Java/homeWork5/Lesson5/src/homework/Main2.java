package homework;

import java.util.Arrays;
import java.util.Scanner;

public class Main2 {
	public static void main(String[] args) { 
	    Scanner sc = new Scanner(System.in); 
	    int size;
	    
	    
	    System.out.println("Enter size ");
	    size = sc.nextInt();
	    int[] arr = new int[size];
	    for(int i = 0; i < size; i++) {
	    	int number;
	    	System.out.println("Enter number ");
	    	number = sc.nextInt();
	    	arr [i] = number;
	    }
	    System.out.println(Arrays.toString(arr));
	}
}
