package homework;

import java.util.Arrays;

public class Main4 {
	public static void main(String[] args) {
		int[] arr = new int[10];

	    for (int i = 0; i < arr.length; i++) {
	      arr[i] = (int) (Math.random() * 10);
	    }
	    System.out.println(Arrays.toString(arr));
	    int startIndex = 0;
	    int endIndex = arr.length - 1;
	    int midleIndex = (startIndex + endIndex)/2;
	    for(int i = startIndex; i <= midleIndex; i++) {
	    	int temp = arr[i];
	    	arr[i] = arr[startIndex + endIndex - i];
	    	arr[startIndex + endIndex - i] = temp;
	    }
	    
	    System.out.println(Arrays.toString(arr));
    }
}
