package homework;

public class Main {

	public static void main(String[] args) {
		int[] arr = new int[] {0,5,2,4,7,1,3,1,9} ;
		int sum = 0;
		
		for (int element : arr) {
			if(element%2 != 0) {
				sum += 1;
			}
		}
		System.out.println("Number of odd numbers - " + sum);
	}

}
