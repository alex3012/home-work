package homework;

import java.util.Arrays;
import java.util.Scanner;

public class Main5 {

	public static void main(String[] args) {
		int [][] arr = new int [5][5];
		int n = arr.length;
		
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < arr[i].length; j++) {
			    arr[i][j] = (int) (Math.random() * 10);
			}
		}
		
		for(int i = 0; i < arr.length; i++) {
			System.out.println(Arrays.toString(arr[i]));
		}
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter rotation angle(90, 180, 270)");
		int degrees = sc.nextInt();
		
		if(degrees == 90) {
			for (int i = 0; i < n / 2; i++) {
	            for (int j = i; j < n - i - 1; j++) {
	                int temp = arr[i][j];
	                arr[i][j] = arr[n - j - 1][i];
	                arr[n - j - 1][i] = arr[n - i - 1][n - j - 1];
	                arr[n - i - 1][n - j - 1] = arr[j][n - i - 1];
	                arr[j][n - i - 1] = temp;
	            }
	        }
		}else if(degrees == 180) {
			for (int i = 0; i < n / 2; i++) {
	            for (int j = i; j < n - i - 1; j++) {
	                int temp = arr[i][j];
	                arr[i][j] = arr[n - i - 1][n - j - 1];
	                arr[n - i - 1][n - j - 1] = temp;
	                temp = arr[j][n - i - 1];
	                arr[j][n - i - 1] = arr[n - j - 1][i];
	                arr[n - j - 1][i] = temp;
	            }
	        }
		}else if(degrees == 270) {
			for (int i = 0; i < n / 2; i++) {
                for (int j = i; j < n - i - 1; j++) {
                    int temp = arr[i][j];
                    arr[i][j] = arr[j][n - i - 1];
                    arr[j][n - i - 1] = arr[n - i - 1][n - j - 1];
                    arr[n - i - 1][n - j - 1] = arr[n - j - 1][i];
                    arr[n - j - 1][i] = temp;
                }
            }
		}else {
			System.out.println("Wrong angle of rotation!");
		}
		
		System.out.println("turn " + degrees);
		
		for(int i = 0; i < arr.length; i++) {
			System.out.println(Arrays.toString(arr[i]));
		}
	}
}
