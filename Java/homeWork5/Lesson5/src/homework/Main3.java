package homework;

import java.util.Arrays;

public class Main3 {

  public static void main(String[] args) {

    int[] arrayOne = new int[15];

    for (int i = 0; i < arrayOne.length; i++) {
      arrayOne[i] = (int) (Math.random() * 100);
    }

    System.out.println(Arrays.toString(arrayOne));
    
    int size = arrayOne.length;
    int[] newArray = Arrays.copyOf(arrayOne, size*2);

    for (int i = 0; i < size; i++) {
      newArray[i+size] = arrayOne[i]*2;
    }

    System.out.println(Arrays.toString(newArray));

  }

}

