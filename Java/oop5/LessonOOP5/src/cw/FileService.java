package cw;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileService {

  public static long copyFile(File fileIn, File fileOut) throws IOException{
    
    try(InputStream is = new FileInputStream(fileIn);
        OutputStream os = new FileOutputStream(fileOut)){
    
      return is.transferTo(os);
      
    }
  }
  
  public static long copyFileInFolder(File folderIn, File folderOut, long maxSize) throws IOException {
	    long fileCopy = 0;
	    File[] files = folderIn.listFiles();
	    for (File file : files) {
	      if (file.isFile() && file.length() <= maxSize) {
	        File fileOut = new File(folderOut, file.getName());
	        copyFile(file, fileOut);
	        fileCopy += 1;
	      }
	    }
	    return fileCopy;
  }
  
  public static long copyFileWithExtension(File folderIn, File folderOut, String extension) throws IOException {
	    long fileCopy = 0;
	    File[] files = folderIn.listFiles();
	    for (File file : files) {
	      if (file.isFile() && file.getName().endsWith(extension)) {
	        File fileOut = new File(folderOut, file.getName());
	        copyFile(file, fileOut);
	        fileCopy += 1;
	      }
	    }
	    return fileCopy;
}
}
