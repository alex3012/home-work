package cw;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.BufferedInputStream;

public class FileComparator {
	public static void main(String[] args) {
		if (args.length < 2) {
			System.out.println("Usage: java FileComparator <file1> <file2>");
			System.exit(1);
		}

		File file1 = new File(args[0]);
		File file2 = new File(args[1]);
		//File file1 = new File("text1.txt");
		//File file2 = new File("text2.txt");

		try {
			boolean areIdentical = compareFiles(file1, file2);
			if (areIdentical) {
				System.out.println("Files are identical.");
			} else {
				System.out.println("Files are different.");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static boolean compareFiles(File file1, File file2) throws IOException {
		try (BufferedInputStream fis1 = new BufferedInputStream(new FileInputStream(file1));
				BufferedInputStream fis2 = new BufferedInputStream(new FileInputStream(file2))) {

			int ch = 0;
			long pos = 1;
			while ((ch = fis1.read()) != -1) {
				if (ch != fis2.read()) {
					return false;
				}
				pos++;
			}
			if (fis2.read() == -1) {
				return true;
			} else {
				return false;
			}
		}
	}
}
