package homework;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import homework.exception.GroupOverflowException;

public class GroupFileStorage {
	public void saveGroupToCSV(Group gr) {
		File file = new File(gr.getGroupName() + ".csv");
		try (PrintWriter pw = new PrintWriter(file)) {
			for (Student student : gr.getStudents()) {
				if (student != null) {
					pw.println(new CSVStringConverter().toStringRepresentation(student));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Group loadGroupFromCSV(File file) {
		String fileName = file.getName();
		String nameWithoutExtension = fileName.substring(0, fileName.lastIndexOf("."));
		Group group = new Group(nameWithoutExtension);

		try (Scanner sc = new Scanner(file)) {
			for (; sc.hasNextLine();) {
				try {
					group.addStudent(new CSVStringConverter().fromStringRepresentation(sc.nextLine()));
				} catch (GroupOverflowException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return group;
	}
	
	public File findFileByGroupName(String groupName, File workFolder) {
		File[] files = workFolder.listFiles();
		for (File file : files) {
			if(file.getName().contains(groupName)) {
				return file;
			}
		}
		System.out.println("There is no such file in this directory"); 
		return null;	
	}
}
