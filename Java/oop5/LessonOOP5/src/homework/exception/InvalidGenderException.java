package homework.exception;

public class InvalidGenderException extends Exception {

	public InvalidGenderException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidGenderException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidGenderException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidGenderException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
