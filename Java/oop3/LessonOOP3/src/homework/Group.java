package homework;

import java.util.Arrays;
import java.util.Comparator;

public class Group {
	private String groupName;
	private final Student[] students;

	public Group(String groupName) {
		super();
		this.groupName = groupName;
		students = new Student[10];
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Student[] getStudents() {
		return students;
	}

	public void addStudent(Student student) throws GroupOverflowException {
		for (int i = 0; i < students.length; i++) {
			if (students[i] == null) {
				students[i] = student;
				return;
			}
		}
		throw new GroupOverflowException("Group is full. Cannot add more students.");
	}

	public Student searchStudentByLastName(String lastName) throws StudentNotFoundException {
		for (int i = 0; i < students.length; i++) {
			if (students[i] != null) {
				if (students[i].getLastName().equalsIgnoreCase(lastName)) {
					return students[i];
				}
			}
		}
		throw new StudentNotFoundException("Student with last name " + lastName + " not found in the group.");
	}

	public boolean removeStudentByID(int id) {
		for (int i = 0; i < students.length; i++) {
			if (students[i] != null) {
				if (students[i].getId() == id) {
					students[i] = null;
					return true;
				}
			}
		}
		return false;
	}

	public void sortStudentsByLastName() {
		Arrays.sort(students, 0, students.length, (s1, s2) -> {
			if (s1 == null || s2 == null) {
				return 0;
			}
			return s1.getLastName().compareTo(s2.getLastName());
		});
	}

	@Override
	public String toString() {
		sortStudentsByLastName();
		String result = "Group" + System.lineSeparator();
		for (int i = 0; i < students.length; i++) {
			if (students[i] != null) {
				result += students[i] + System.lineSeparator();
			}
		}

		return result;
		// return "Group [groupName=" + groupName + ", students=" +
		// Arrays.toString(students) + "]";
	}

}
