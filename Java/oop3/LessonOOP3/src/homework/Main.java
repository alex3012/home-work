package homework;

public class Main {

	public static void main(String[] args) {
		Student student1 = new Student("Djon", "BOW", Gender.MALE, 1, "BACK-END");
		Student student2 = new Student("Dik", "Svenson", Gender.MALE, 3, "BACK-END");
		Student student3 = new Student("Anna", "Smitt", Gender.FEMALE, 9, "BACK-END");
		Student student4 = new Student("Alan", "Po", Gender.MALE, 6, "BACK-END");
		Student student5 = new Student("Ray", "Bredbery", Gender.MALE, 8, "BACK-END");
		Student student6 = new Student("Don", "Nilson", Gender.OTHER, 7, "BACK-END");
		Student student7 = new Student("Janna", "Vo", Gender.FEMALE, 5, "BACK-END");
		Student student8 = new Student("Patrik", "Zimmermann", Gender.MALE, 4, "BACK-END");
		Student student9 = new Student("Alex", "Niman", Gender.MALE, 10, "BACK-END");
		Student student10 = new Student("Mik", "Tarkov", Gender.MALE, 2, "BACK-END");
		
		Group group = new Group("BACK-END");
		
		try {
			group.addStudent(student1);
			group.addStudent(student2);
			group.addStudent(student3);
			group.addStudent(student4);
			group.addStudent(student5);
			group.addStudent(student6);
			group.addStudent(student7);
			group.addStudent(student8);
			group.addStudent(student9);
			group.addStudent(student10);
			group.addStudent(new Student("Bob", "Markov", Gender.MALE, 2, "BACK-END"));
			
		} catch (GroupOverflowException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			group.searchStudentByLastName("Box");
		} catch (StudentNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(group.removeStudentByID(4));
		System.out.println(group.removeStudentByID(11));
		
		System.out.println(group);

	}

}
