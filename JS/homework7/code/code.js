/*Створити клас Animal та розширюючі його класи Dog, Cat, Horse.
Клас Animal містить змінні food, location і методи makeNoise, eat, sleep. Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить".
Dog, Cat, Horse перевизначають методи makeNoise, eat.
Додайте змінні до класів Dog, Cat, Horse, що характеризують лише цих тварин.
Створіть клас Ветеринар, у якому визначте метод void (Нічого не повертає) treatAnimal(Animal animal). Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.
У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас. У циклі надсилайте їх на прийом до ветеринара.*/


function Animal (food, location){
    this.food = food;
    this.location = location;
}

Animal.prototype.maceNoise = function () {
    return document.write(`Така тварина спить<br/>`)
 };
 Animal.prototype.eat = function(){
    return document.write(`Така тварина їсть ${this.food}<br/>`)
 };
 Animal.prototype.sleep = function(){
    return document.write(`Ця тварина живе в такому місці: ${this.location}<br/>`)
 };

const animal = new Animal;

function Dog (food, location){
    this.food = food;
    this.location = location;
    this.dogBreed = "Amstaff";
    this.maceNoise = function () {
        return document.write(`Цей пес живе у місті ${this.location}<br/>`)
     };
     this.eat = function(){
        return document.write(` полюбляє їсти ${this.food}<br/>`)
     };
};

Dog.prototype = animal

function Cat (food, location){
    this.food = food;
    this.location = location;
    this.catBreed = "Pers";
    this.maceNoise = function () {
        return document.write(`Цей кіт живе у місті ${this.location}<br/>`)
     };
     this.eat = function(){
        return document.write(` полюбляє їсти ${this.food}<br/>`)
     };
};

Cat.prototype = animal

function Horse (food, location){
    this.food = food;
    this.location = location;
    this.horseBreed = "Abaco Barb";
    this.maceNoise = function () {
        return document.write(`Цей кінь живе у місті ${this.location}<br/>`)
     };
     this.eat = function(){
        return document.write(` полюбляє їсти ${this.food}<br/>`)
     };
};

Horse.prototype = animal


const staf = new Dog(`м'ясо`,`Київ`);
const pers = new Cat(`риба`,`Запоріжжя`);
const abaco = new Horse(`овес`,`Тернопіль`);

//Перевірка

document.write(staf.dogBreed)
staf.eat();
staf.maceNoise();

document.write(`<hr/>`)

document.write(pers.catBreed)
pers.eat();
pers.maceNoise();

document.write(`<hr/>`)

document.write(abaco.horseBreed)
abaco.eat();
abaco.maceNoise();

document.write(`<hr/>`)

function Ветеринар() {}

Ветеринар.prototype.void = function(){}
Ветеринар.prototype.treatAnimal = function(arg){
 document.write(`${arg.food},    ${arg.location}<br>`)
}
Ветеринар.prototype.main = function(arrAnimals = []){
    arrAnimals.forEach((animal) => {
        new Ветеринар().treatAnimal(animal);
    })
}



const animals = [staf, pers, abaco];

new Ветеринар().main(animals);
