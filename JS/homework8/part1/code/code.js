function createTask(num){
   const container = document.createElement(`section`);
   container.classList.add(`container${num}`);
   document.body.append(container)
}

createTask(1);
//Створіть 2 інпути та одну кнопку. Зробіть так, щоб інпути обмінювалися вмістом.

const input1 = document.createElement(`input`),
input2 = document.createElement(`input`),
btn = document.createElement(`button`);
input1.value = `input1`;
input2.value = `input2`;

btn.innerText = `Change`;

document.querySelector(`.container1`).append(input1, input2, btn);

btn.onclick = function() {
    const a1 = input1.value;
    const a2 = input2.value
    input1.value = a2;
    input2.value = a1;  
} 

document.write(`<hr/>`);

createTask(2);
//Створіть 5 див на сторінці потім використовуючи getElementsByTagName і forEath поміняйте дивам колір тла.

for(i = 0; i < 5; i++){
    const div = document.createElement(`div`);
    div.textContent = `div${i+1}`;
    document.querySelector(`.container2`).append(div)
}

const btn2 = document.createElement(`button`);
btn2.textContent = `Change color`;
document.querySelector(`.container2`).append(btn2);

const [...divs] =  document.getElementsByTagName("div"); 

btn2.onclick = function() {
    divs.forEach(element => {
    element.style.backgroundColor = "red"
});  
}


document.write(`<hr/>`);

createTask(3)
/*Створіть багаторядкове поле для введення тексту та кнопки. Після натискання кнопки користувачем програма повинна
згенерувати тег div з текстом, який був у багаторядковому полі. багаторядкове поле слід очистити після
переміщення інформації*/

const textarea = document.createElement(`textarea`);
textarea.value = `I like to program on Java Script`;

const btn3 = document.createElement(`button`);
btn3.innerText = `Press`

document.querySelector(`.container3`).append(textarea, btn3)

btn3.onclick = function() {
    const div3 = document.createElement(`div`);
    div3.textContent = textarea.value;
    textarea.value = ``;
    document.querySelector(`.container3`).append(div3);
    btn3.setAttribute(`disabled`,``);
}

document.write(`<hr/>`);

createTask(4)
/*Створіть картинку та кнопку з назвою "Змінити картинку"
зробіть так щоб при завантаженні сторінки була картинка
https://itproger.com/img/courses/1476977240.jpg
При натисканні на кнопку вперше картинка замінилася на
https://itproger.com/img/courses/1476977488.jpg
при другому натисканні щоб картинка замінилася на
https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png*/

const sRc = [`https://itproger.com/img/courses/1476977240.jpg`, `https://itproger.com/img/courses/1476977488.jpg`, `https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png`]; 

const img = document.createElement(`img`);
img.setAttribute(`src`,`${sRc[0]}`);

const btn4 = document.createElement(`button`);
btn4.textContent = `Change image`;

document.querySelector(`.container4`).append(img, btn4);

let j = 0;

btn4.onclick = function() {
    j++;
    if(j < sRc.length){
        img.setAttribute(`src`,`${sRc[j]}`);
    }    
}

document.write(`<hr/>`);

 createTask(5)       
// Створює на сторінці 10 парахрафів і зробіть так, щоб при натисканні на параграф він зникав

for(i = 0; i < 10; i++){
    const paragraf = document.createElement(`p`);
    paragraf.textContent = `paragraf${i+1}`;
    document.querySelector(`.container5`).append(paragraf)
    paragraf.onclick = function(){
    paragraf.remove()
    }
} 
