const pizza = {
    size: {
       size: 85
    },
    sauce: {},
    topping: {}
}

const price = {
    size: {
        small: 50,
        mid: 75,
        big: 85
    },
    sauce:{
        sauceClassic: 4,
        sauceBBQ: 5,
        sauceRikotta: 6  
    },
    topping: {
        moc1: 2,
        moc2: 3,
        moc3: 4,
        telya: 5,
        vetch1: 6,
        vetch2: 7
    }
}

window.addEventListener("DOMContentLoaded", () => {
     // Шукаємо розмір коржу
    document.getElementById("pizza").addEventListener("click", function (ev) {
         //Визначаємо обраний корж і записуємо данні в обьєкт 
        switch (ev.target.id) {
            case "small": pizza.size.size = price.size.small;
                break
            case "mid": pizza.size.size = price.size.mid;
                break
            case "big": pizza.size.size = price.size.big;
                break
        }
        show(pizza)
    })
    
    //обираємо добавки

    let sauceDrag = null;
    let sWitch = false;
    const saucesList = document.querySelector(`#sauce`);
    const toppingList = document.querySelector(`#topping`);
    const table = document.querySelector('.table'); 

    document.querySelectorAll(`.draggable`).forEach(element => {
        element.addEventListener('dragstart', () => {
            sauceDrag = element.cloneNode();
            sauceDrag.id = `${element.id}_2`;    
        })
        
        element.addEventListener("dragend", () => {
            if(sWitch) {
                element.classList.add(`noActive`);
                let sauce = element.nextElementSibling.cloneNode(true);
                sauce.classList.add(`${element.id}`);
                if(element.classList.contains(`typeOfsouse`)) {
                    saucesList.append(sauce);
                    pizza.sauce[`${element.id}`] = price.sauce[`${element.id}`];
                }
                if(element.classList.contains(`typeOftopping`)) {
                    toppingList.append(sauce);
                    pizza.topping[`${element.id}`] = price.topping[`${element.id}`];
                }
                show(pizza) 
                sauce.addEventListener(`click`, () => {
                    sauceRemove(sauce);
                    show(pizza)
                })    
            }
            sWitch = false;    
        });    
    }); 
        
    table.addEventListener("dragenter", () => {   
    });
    table.addEventListener("dragleave", () => {    
    });
    table.addEventListener("dragover", (evt) => {
        evt.preventDefault();
    });
    table.addEventListener("drop", (evt) => {
        evt.stopPropagation();
        table.appendChild(sauceDrag);
        sWitch = true;
    });
    
    show(pizza);
    btnRun();
});
//видалення інгрідієнта
const sauceRemove = (sauce) => {
    sauce.remove();
    document.querySelector(`#${sauce.className}`).classList.remove(`noActive`);
    document.querySelector(`#${sauce.className}_2`).remove();
    if(pizza.sauce.hasOwnProperty(`${sauce.className}`)) delete pizza.sauce[`${sauce.className}`];
    if(pizza.topping.hasOwnProperty(`${sauce.className}`)) delete pizza.topping[`${sauce.className}`];
    show(pizza);
}

//метод для виводу інформації про продукт
function show (pizza) {
    //отримали блок ціни
    const priCe = document.querySelector("#price");
    let newObject = {
        ...pizza.size,
        ...pizza.sauce,
        ...pizza.topping
    }
    
    priCe.innerText = Object.values(newObject).reduce((a, b) => a + b, 0);
}

function btnRun () {
    
    const btn = document.querySelector("#banner");
    
    btn.addEventListener("mousemove", () => {
        const coords = {
            X : Math.floor(Math.random() * document.body.clientWidth),
            Y : Math.floor(Math.random() * document.body.clientHeight)
        }
        
        if((coords.X + 350) > document.body.clientWidth){
            return
        }
        if((coords.Y + 150) > document.body.clientHeight){
            return
        }
        btn.style.top = coords.Y + "px"
        btn.style.left = coords.X + "px"
    })
}




