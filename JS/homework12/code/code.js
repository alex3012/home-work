const userName = document.querySelector(`#info input[name="name"]`),
      userFhone = document.querySelector(`#info input[name="phone"]`),
      userEmail = document.querySelector(`#info input[name="email"]`),
      btnReset = document.querySelector(`#info input[name="cancel"]`),
      btnSubmit = document.querySelector(`#btnSubmit`);

userName.addEventListener("change", (ev) => {
    const p = /^[а-яіґєї]{2,}$/i
    if( p.test(ev.target.value)){
        userData.name = ev.target.value;
        ev.target.className = ""
    }else{
        ev.target.classList.add("error")
    }

})

userFhone.addEventListener("change", (ev) => {
    const p = /^\+380\d{9}$/
    if(p.test(ev.target.value)){
        userData.fhone = ev.target.value;
        ev.target.className = ""
    }else{
        ev.target.classList.add("error")
    }
})
userEmail.addEventListener("change", (ev) => {
    const p = /^[a-z0-9_.]{3,}@[a-z.0-9]{2,}\.[a-z.]{2,10}$/i
    if(p.test(ev.target.value)){
        userData.email = ev.target.value;
        ev.target.className = ""
    }else{
        ev.target.classList.add("error")  
    }
})

btnReset.addEventListener(`click`, () => {
    if(confirm(`Скинути дані?`)){
        userName.classList.remove("error");
        userFhone.classList.remove("error");
        userEmail.classList.remove("error");
    }
})

btnSubmit.addEventListener(`click`, () => {
    if(!userName.classList.contains(`error`) && !userFhone.classList.contains(`error`) && !userEmail.classList.contains(`error`) && userName.value !== `` && userFhone.value !== `` && userEmail.value !== ``) {
        window.location.href = `./thank-you/index.html`;
    }else{
        alert(`Заповніть форму`);
    }
})

const userData = {

}