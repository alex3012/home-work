/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/

const keys = document.querySelector(`.keys`);
const display = document.querySelector(`.display`).firstElementChild;
const displayM = document.querySelector(`.display`);
let a = ``, v1 = ``, v2 = ``, actionSign = ``, m = ``, mRc = false;
display.getAttribute;

const action = () => {
    if (a != ``){
        v1 = parseFloat(a);
        a = ``;   
    }
    if(display.value != ``){
        document.querySelector(`#eq`).removeAttribute(`disabled`)
    }
}

const result = () => {
    if(actionSign != `` && a != ``){
        v2 = parseFloat(a);
        a = ``;
    }else if(actionSign != `` && a === ``){
        v2 = v1;
    }   
}

keys.addEventListener("click", (e) => {
    if(e.target.classList.contains(`keys`)){
        return
    }
    //введення числа  
    if (e.target.classList.contains(`black`)) {
        if(e.target.value === `.`){
            const pattern = /[.]/;
            if(a != `` && !(pattern.test(a))){
                a += e.target.value;
            }       
        }
        if(e.target.value != `.` && e.target.value != `C`){
            if(a == `0`){
                a = ``;   
            }
            a += e.target.value;
        }
        //reset
        if(e.target.value === `C`){
            if(a != ``){
                a = ``
            }else{
                v1 = ``, v2 = ``, actionSign = ``;
            }
        }
        display.value = a
    }
    //дія
    if(e.target.classList.contains(`pink`)){
        actionSign = e.target.value;
        action();
    }
    //результат
    if(e.target.classList.contains(`orange`)){
        result();
        
        if(actionSign === `-`){
            a = v1 - v2;
        }else if(actionSign === `+`){
            a = v1 + v2;
        }else if(actionSign === `*`){
            a = v1 * v2;
        }else if(actionSign === `/`){
            if(v2 != 0){
                a = v1 / v2;
            }else{
                a = ``;
            } 
        }    
        display.value = a;
        v1 = parseFloat(a);
        a = ``;
        document.querySelector(`#eq`).setAttribute(`disabled`,`true`)
    }
    //memory
    if(e.target.classList.contains(`gray`)){
        if(e.target.value === `m+`){
            m = display.value;
            mRc = true;
            displayM.classList.add(`memory`);
        }
        if(e.target.value === `m-`){
            m = -display.value;
            mRc = true;
            displayM.classList.add(`memory`);
        }
        if(e.target.value === `mrc`){
            if(mRc){
                a = m;
                mRc = false;
                display.value = a;
            }else{
                m = ``;
                displayM.classList.remove(`memory`);
            }

        }
    }
}, false);
 