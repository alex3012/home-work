const pagination = document.querySelector(`.pagination`);
const comments = document.querySelector(".comment");
let data = [];
let pageNum;
let pageValue;
let page;

function paginationShow () {
    pageNum = Math.ceil(data.length / 10);
    let string = `<span><</span>`;
    pagination.insertAdjacentHTML("beforeend", string);
    for(let i = 1; i <= pageNum; i++){
        string = `<span class = "paginationNam">${i}</span>`;
        pagination.insertAdjacentHTML("beforeend", string);
    }
    string = `<span>></span>`;
    pagination.insertAdjacentHTML("beforeend", string);
    page = pagination.firstElementChild.nextElementSibling;
    pageValue = page.textContent;
    page.classList.add(`active_el`);
}

function deletePage () {
    [...comments.children].forEach( (el) => {
        el.remove()
    })
}

function paginationActive (el) {
    if(el.textContent != `<` && el.textContent != `>`){
        page.classList.remove(`active_el`);
        el.classList.add(`active_el`); 
    }
    if(el.textContent === `<`){
        page.classList.remove(`active_el`);
        page.previousElementSibling.classList.add(`active_el`);
    }
    if(el.textContent === `>`){
        page.classList.remove(`active_el`);
        page.nextElementSibling.classList.add(`active_el`);
    }
}

function paginationChoice (el) {
    if(el != page && el.textContent != `<` && el.textContent != `>`) {
        paginationActive(el);
        page = el;
        pageValue = page.textContent;
        deletePage();
        show(data, pageValue);
        return;
    }
    if(el.textContent === `>` && pageValue < pageNum) {
        paginationActive(el);
        page = page.nextElementSibling;
        pageValue = page.textContent;
        show(data, pageValue);
        return;
    }
    if(el.textContent === `<` && pageValue > 1) {
        paginationActive(el);
        page = page.previousElementSibling;
        pageValue = page.textContent;
        show(data, pageValue);
        return;
    }
}

function req(method = "GET", url = "", callback) {
    document.querySelector(".box-loader").classList.add("active")
    const r = new XMLHttpRequest();

    r.open(method, url);
    r.send();
    r.addEventListener("readystatechange", () => {
        if (r.readyState === 4 && r.status >= 200 && r.status < 300) {
            setTimeout(() => {
                data = JSON.parse(r.responseText);
                paginationShow();
                callback(data, pageValue);
                document.querySelector(".box-loader").classList.remove("active")
            }, 1500)
            
        } else if (r.readyState === 4) {
            console.error("Помилка з запитом")
        }
    })
}

const show = (arr, number) => {
    deletePage ()
    arr.forEach((el, i) => {
        if(i < (number*10) && i > (number *10 - 11)){
        const string = `
        <div>
            <span class = "id">${el.id}.</span>
            <span class = "hed"><span class = "name">${el.name}</span><span class = "email">${el.email}</span>
                <span>Дата коментаря: 10.10.2022</span></span>
            <div class = "text">${el.body}</div>
            <a href="">Відповісти</a>
        </div>
       `
       comments.insertAdjacentHTML("beforeend", string)
       }
    })
}

pagination.addEventListener(`click`, (event) => {
   paginationChoice(event.target) 
})

req("GET", "https://jsonplaceholder.typicode.com/comments", show)
