/*  Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.
https://storage.googleapis.com/www.examclouds.com/oop/car-ierarchy.png  */

class Driver {
    constructor(fullName, experience) {
        this.fullName = fullName;
        this.experience = experience;
    }
}

class Engine {
    constructor(power, company) {
        this.power = power;
        this.company = company;
    }
}

class Car {
    constructor(marka, klass, weight, driver, engine) {
        this.marka = marka;
        this.klass = klass;
        this.weight = weight;
        this.driver = driver;
        this.engine = engine;
    }
    start(){
        document.write("Поїхали");
    }
    stop(){
        document.write("Зупиняємося"); 
    }
    turnRight(){
        document.write("Поворот праворуч");
    } 
    turnLeft(){
        document.write("Поворот ліворуч");
    };
    toString(){
        document.write(`Модель: ${this.marka},<br> Тип: ${this.klass},<br> Вага: ${this.weight} кг.,<br> П.І.Б.: ${this.driver.fullName},<br> Стаж: ${this.driver.experience} р.,<br> Потужність: ${this.engine.power} к.с.,<br> Виробник: ${this.engine.company}`)
    }
}

class Lorry extends Car{
    constructor(marka, klass, weight, driver, engine, carryingCapacity) {
        super(marka, klass, weight, driver, engine);
        this.carryingCapacity = carryingCapacity;
    }
    toString(){
        document.write(`Модель: ${this.marka},<br> Тип: ${this.klass},<br> Вага: ${this.weight} кг.,<br> П.І.Б.: ${this.driver.fullName},<br> Стаж: ${this.driver.experience} р.,<br> Потужність: ${this.engine.power} к.с.,<br> Виробник: ${this.engine.company},<br> Вантажопідйомність: ${this.carryingCapacity} кг.`)
    }

}

class SportCar extends Car{
    constructor(marka, klass, weight, driver, engine, speed) {
        super(marka, klass, weight, driver, engine);
        this.speed = speed;
    }
    toString(){
        document.write(`Модель: ${this.marka},<br> Тип: ${this.klass},<br> Вага: ${this.weight} кг.,<br> П.І.Б.: ${this.driver.fullName},<br> Стаж: ${this.driver.experience} р.,<br> Потужність: ${this.engine.power} к.с.,<br> Виробник: ${this.engine.company},<br> Max. швидкість: ${this.speed} км/г.`)
    }
}

//Приклади

const driver1 = new Driver(`Безхатько Петро Григорович`, 15);

const engine1 = new Engine(300, `General Motors`);
const car1 = new Car(`Cadillak`, `Sedan`, 1500, driver1, engine1);
car1.toString();
document.write(`<hr></hr>`)

const engine2 = new Engine(350, `General Motors`)
const lorry1 = new Lorry(`Chevrolet Kodiak`, `Truck`, 3500, driver1, engine2, 14500);
lorry1.toString();
document.write(`<hr></hr>`)

const engine3 = new Engine(670, `General Motors`)
const sportCar1 = new SportCar(`Corvette`, `Coupe`, 1300, driver1, engine3, 312);
sportCar1.toString();