const divContainer = document.querySelector(`.container-stopwatch`),
      divDisplay = document.querySelector(`.stopwatch-display`),
      divControl = document.querySelector(`.stopwatch-control`),
      span = document.querySelectorAll(`span`),
      button = document.querySelectorAll(`button`);
      

span[0].classList.add(`hours`);
span[1].classList.add(`minutes`);
span[2].classList.add(`seconds`);

button[0].classList.add(`start`);
button[1].classList.add(`stop`);
button[2].classList.add(`reset`);

console.log(button);

const replace = (el, color) => {
    el.classList.remove(`black`, `red`, `silver`, `green`);
    el.classList.add(color);
}
let sec = 0, min = 0, h =0;

const secondTimer = (time) => {
    sec++;
    document.querySelector(".seconds").textContent = time;
    if(sec == 60){
        sec =0;
        min++;
    }
}

const minuteTimer = (time) => {
    document.querySelector(".minutes").textContent = time;
    if(min == 60){
        min = 0;
        h++;
    }
}

const hourTimer = (time) => {
    document.querySelector(".hours").textContent = time;
    if(h == 24){
        h = 0;
    }
}

const timer = () => {
    let second = (sec < 10) ? "0" + sec : sec;
    secondTimer(second);
    let minute = (min < 10) ? "0" + min : min;
    minuteTimer(minute);
    let hour = (h < 10) ? "0" + h : h;
    hourTimer(hour);  
}

let start;

const press = selector => document.querySelector(selector);

press(".start").onclick = () => {
    replace(divContainer, `green`);
    start = setInterval(timer, 1000);
}

press(".stop").onclick = () => {
    replace(divContainer, `red`)
    clearInterval(start);
}

press(".reset").onclick = () => {
    replace(divContainer, `silver`);
    clearInterval(start);
    sec = 0;
    min = 0;
    h =0;
    secondTimer(`00`);
    minuteTimer(`00`);
    hourTimer(`00`);
}