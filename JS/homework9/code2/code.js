const part2 = document.createElement(`div`);
part2.classList = `part2`;
part2.style.marginTop = `4vh`;
divContainer.after(part2);

const numberPhone = document.createElement(`input`)
numberPhone.classList = `numberPhone`;
numberPhone.placeholder = `0ХХ-ХХХ-ХХ-ХХ`;
numberPhone.classList.add(`container-stopwatch`, `red`)
part2.append(numberPhone);

const buttonSave = document.createElement(`button`);
buttonSave.classList.add(`silver`);
buttonSave.textContent = `Save`;
part2.append(buttonSave);

const error = document.createElement(`div`);
error.classList = `error`
error.classList.add(`container-stopwatch`, `red`);
error.textContent = `Номер введено невірно`;


const pattern = /^0\d{2}-\d{3}-\d{2}-\d{2}$/;

buttonSave.onclick = () => {
    if(pattern.test(numberPhone.value)){
        part2.classList.add(`green`)
        setTimeout(() => {
            document.location = `https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg`
        }, 3000);
        if(document.querySelector(`.error`)){
            error.remove();
        }
    }else{
        part2.prepend(error);
    }
}

