/*
2.
Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів. Скільки символів має бути в інпуті, зазначається в атрибуті data-length. Якщо вбито правильну кількість, то межа інпуту стає зеленою, якщо неправильна – червоною.

*/
const [...inputs] = document.querySelectorAll(`input`);

inputs.forEach(element => {
    element.addEventListener("focus", (ev) => {
        ev.target.classList.remove("valid", "error");  
    })

    element.addEventListener("blur", (ev) => {
        ev.target.className = "";
        if(ev.target.value.length == ev.target.dataset.length){
            ev.target.classList.add("valid");   
        }else{
            ev.target.classList.add("error");
        }
    })
})
