/*Реалізуйте клас MyString, який матиме такі методи: метод reverse(),
який параметром приймає рядок, а повертає її в перевернутому вигляді, метод ucFirst(),
який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою
та метод ucWords, який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.*/

function MyString(line) {
    this.line = line;
}

MyString.prototype.reverse = function () {
    let result = this.line.split(``).reverse().join(``);
    return result;
}

MyString.prototype.ucFirst = function () {
    return this.line.slice(0,1).toUpperCase() + this.line.slice(1);
}

MyString.prototype.ucWords = function () {
    let result = this.line.split(` `).map(function(element){
       return element.slice(0,1).toUpperCase() + element.slice(1);
    }).join(` `);
    return result;
}

const nMyString = new MyString(`i like my job`);


document.write(`${nMyString.reverse()}<br>`);

document.write(`${nMyString.ucFirst()}<br>`);

document.write(`${nMyString.ucWords()}<br>`);