/*Реалізуйте клас Worker (Працівник), який матиме такі властивості: name (ім'я), surname (прізвище),
rate (ставка за день роботи), days (кількість відпрацьованих днів).
Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника.
Зарплата - це добуток (множення) ставки rate на кількість відпрацьованих днів days.*/

function Worker(name, surname, rate, days) {
    this.name = name;
    this.surname = surname;
    this.rate = rate;
    this.days = days;
}

Worker.prototype.getSalary = function () {
    document.write(`Робітник ${this.name} ${this.surname} має зарплатню ${this.rate * this.days}`)
}

const nWorker = new Worker(`Іван`, `Іванов`, 100, 20);
nWorker.getSalary();